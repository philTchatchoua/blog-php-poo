<?php
/*
* 	 Www/index.php
* 	 Dispacher centrale
*/

   //Chargement des fichier d'intialisation
      require_once'../App/Config/parametres.php';
      require_once'../vendor/autoload.php';

   //Demarrage del'application
      \Noyau\Classes\App::start();
      
   //Chargement du routeur
      require_once'../App/routeur.php';

   //Chargement du template
   //\Noyau\Classes\App::getTemplate();
      require_once'../App/Vues/Templates/default.php';
   
   //Fin de l'application
      \Noyau\Classes\App::close();


  