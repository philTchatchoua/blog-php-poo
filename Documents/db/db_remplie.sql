-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_mvc_poo
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actualites`
--

DROP TABLE IF EXISTS `actualites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `texte` varchar(45) NOT NULL,
  `dateCreation` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actualites`
--

LOCK TABLES `actualites` WRITE;
/*!40000 ALTER TABLE `actualites` DISABLE KEYS */;
INSERT INTO `actualites` VALUES (1,'language php',' Lorem ipsum dolor sit amet, consectetur adip','2019-12-13 00:00:00'),(2,'Bases de Donnees Mysql','Lorem ipsum dolor sit amet, consectetur adipi','2019-12-14 14:24:21'),(3,'Packages et plugins','Tempor id eu nisl nunc mi ipsum. Nunc mattis ','2019-12-14 14:24:30'),(4,'le gestionnaire Generique','Nunc consequat interdum varius sit amet matti','2019-12-14 14:24:37');
/*!40000 ALTER TABLE `actualites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `texte` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Editorial','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae diam id eros gravida convallis. Curabitur ultricies est metus, sit amet fermentum metus pretium eu. In hac habitasse platea dictumst. Etiam ullamcorper elit a lacus tincidunt, ut maximus libero ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec ac risus et sem condimentum sollicitudin vel id est. Donec sed augue dictum, mattis ipsum id, porttitor nibh. Donec eget mauris sapien. Donec rhoncus nibh nunc, sit amet auctor turpis lobortis non. Phasellus non nunc non nisl tincidunt eleifend quis at leo. Phasellus maximus accumsan mi sed aliquam. Maecenas vel accumsan mi, at ultricies diam. In hac habitasse platea dictumst. '),(2,'Produits','Duis quis quam non tortor pellentesque pharetra. Integer maximus eleifend felis id volutpat. Integer quam erat, pulvinar ut iaculis ac, cursus ut eros. Vivamus suscipit lobortis dui. Aliquam erat volutpat. Fusce lacinia suscipit sollicitudin. Phasellus ut mollis tortor. Phasellus ut augue posuere, luctus lectus a, egestas felis. Duis sit amet lobortis sem. Sed pharetra quis mauris vel pellentesque. Phasellus tincidunt arcu ac lectus pellentesque, sed dignissim leo egestas. Curabitur eget nisl augue. Quisque blandit porttitor lorem at semper. Nunc auctor feugiat consequat.Suspendisse et felis tristique, dapibus quam ut, fermentum neque. Donec elementum placerat urna, eu aliquet justo auctor sit amet. Nam sodales dolor malesuada laoreet fermentum. Nunc sed porta orci. Nullam consequat dui vel lectus hendrerit congue. Aliquam feugiat sodales tortor sit amet ullamcorper. Donec tortor lorem, dignissim at sollicitudin sed, efficitur ut odio. Fusce mattis quis arcu vel elementum. Aenean placerat neque a nulla tincidunt lacinia'),(3,'Contacts',' Duis quis quam non tortor pellentesque pharetra. Integer maximus eleifend felis id volutpat. Integer quam erat, pulvinar ut iaculis ac, cursus ut eros. Vivamus suscipit lobortis dui. Aliquam erat volutpat. Fusce lacinia suscipit sollicitudin. Phasellus ut mollis tortor. Phasellus ut augue posuere, luctus lectus a, egestas felis. Duis sit amet lobortis sem. Sed pharetra quis mauris vel pellentesque. Phasellus tincidunt arcu ac lectus pellentesque, sed dignissim leo egestas. Curabitur eget nisl augue. Quisque blandit porttitor lorem at semper. Nunc auctor feugiat consequat.\n\nProin quis mi non tellus elementum lobortis at a urna. Sed eu arcu sed nibh ultricies iaculis. Morbi fringilla in neque scelerisque aliquet. Duis sagittis ac justo quis blandit. Vestibulum tincidunt bibendum cursus. Suspendisse iaculis euismod felis, non ultrices nibh congue in. In at malesuada sem. In diam nulla, iaculis sed laoreet a, bibendum quis magna. Nunc sed finibus arcu, eget rhoncus nunc. In sodales sodales ipsum, sed commodo nisi. Cras id urna imperdiet mauris rutrum vulputate eget vitae mi. ');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-14 15:14:12
