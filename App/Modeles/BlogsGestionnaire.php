<?php

/*
*   ../App/Modeles/BlogsGestionnaire.php
*    Gestionnaire des blogs
*/

namespace App\Modeles;

use \Noyau\Classes\App;

class BlogsGestionnaire extends \Noyau\Classes\GestionnaireGenerique
{

    public function __construct()
    {
        $this->_table = 'blogs';
        parent::__construct();
    }
}
