<?php

/*
*   ../App/Modeles/postsModele.php
*   Modeles - posts
*/

namespace App\Modeles;

use \Noyau\Classes\App;

class ActualitesGestionnaire extends \Noyau\Classes\GestionnaireGenerique
{

    public function __construct()
    {
        $this->_table = 'actualites';
        parent::__construct();
    }
}
