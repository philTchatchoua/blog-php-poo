<?php
/*
*   ../App/Controleurs/postsControleur.php
*   Controleur - Posts
*/

namespace App\Controleurs;

class BlogsControleur extends \Noyau\Classes\ControleurGenerique
{

    //constructeur

    public function __construct()
    {
        $this->_tableName = 'blogs';
        parent::__construct();
    }
}
