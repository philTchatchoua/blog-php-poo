<?php
/*
    *   ../App/Vues/Posts/show.php
    *   Details d'une Actualites
    *   Variable disponible => $Actualites Array(id, titre, texte)
    */

use \Noyau\Classes\Template;
use \Noyau\Classes\App;
?>

<!-- Affectation du contenu de la zonz 'titre' -->
<?php
Template::startZone();
echo $titre = $actualite->getTitre();
Template::stopZone('titre');
?>
<!-- Affectation du contenu de la zonz 'titre' -->
<?php Template::startZone(); ?>
<h1><?php echo $actualite->getTitre(); ?></h1>
<time><?php echo $actualite->getDateCreation(); ?></time>

<br>
<div>
    <?php echo $actualite->getTexte(); ?>
</div>
<?php Template::stopZone('content'); ?>