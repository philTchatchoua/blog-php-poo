<?php
/*
	*  ../App/Vues/Templates/Partials/aside.php
	*  partie aside de template par default
	*/
?>

<div class="sidebar">
	<!-- Blog Search Well -->
	<!-- Subscription widget -->
	<div class="card-panel">
		<div class="row">
			<div class="col-md-12">
				<h4>Actualités</h4><br>
				<?php 
					$ctrl = new \App\Controleurs\ActualitesControleur();
					$ctrl->indexAction();
				?>
				
			</div>
		</div>
	</div>
	<!--/.Subscription widget -->
</div>