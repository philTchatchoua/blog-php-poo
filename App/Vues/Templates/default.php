<?php 
	/*
	 * ../App/Vues/Templates/default.php
	 * -- templete par default 
	 */
 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<?php include'../App/Vues/Templates/Partials/head.php';?>
 </head>

<body>
  <!-- Navigation -->
  	<?php include'../App/Vues/Templates/Partials/nav.php';?>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-8">
			<?php echo $content; ?>
      </div>

      <!-- Blog Sidebar Widgets Column -->
      <div class="col-md-4">
		<?php include'../App/Vues/Templates/Partials/aside.php';?>
      </div>
    </div>
    <!-- /.row -->

    <hr>

  </div>
  <!-- /.container -->


  <!-- Footer -->
  	<?php include'../App/Vues/Templates/Partials/footer.php';?>


  <!-- SCRIPTS -->
  <!-- JQuery -->
	<?php include'../App/Vues/Templates/Partials/scripts.php';?>

</body>

</html>


