<?php
/*
    *   ../App/Vues/Posts/menu.php
    *   Menu de l'application
    *   Variable disponible => $pages Array(OBJ page(id, titre, texte))
    */

?>
<ul class="nav navbar-nav">
    <?php foreach ($pages as $page) : ?>
        <li>
            <a href="page/<?php echo $page->getId(); ?>">
                <?php echo $page->getTitre(); ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>