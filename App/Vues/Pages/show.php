<?php
/*
    *   ../App/Vues/Posts/show.php
    *   Details d'une page
    *   Variable disponible => $pages Aray(id, titre, texte)
    */

use \Noyau\Classes\Template;
?>

<!-- Affectation du contenu de la zonz 'titre' -->
<?php
    Template::startZone();
    echo $titre = $page->getTitre();
    Template::stopZone('titre');
?>
<!-- Affectation du contenu de la zonz 'titre' -->
<?php Template::startZone(); ?>
    <h1><?php echo $page->getTitre(); ?></h1>
    <div>
        <?php echo $page->getTexte(); ?>
    </div><br>
   
<?php Template::stopZone('content'); ?>
 