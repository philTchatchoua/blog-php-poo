<?php
/*
	*  ../App/Vues/Blogs/index.php
	*
    */

use \Noyau\Classes\Template;
?>
<?php Template::startZone(); ?>
<ul>
    <?php foreach ($blogs as $blog) : ?>
        <li>
            <a href="blog/<?php echo $blog->getId(); ?>">
                <?php echo $blog->getTitre(); ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
<?php Template::stopZone('content'); ?>