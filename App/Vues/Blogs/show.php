<?php
/*
    *   ../App/Vues/Blogs/show.php
    *   Details d'un blog
    *   Variable disponible => $blogs Array(id, titre, texte, dateCreation)
    */

use \Noyau\Classes\Template;
use \Noyau\Classes\App;
?>

<!-- Affectation du contenu de la zonz 'titre' -->
<?php
    Template::startZone();
    echo $titre = $blog->getTitre();
    Template::stopZone('titre');
?>
<!-- Affectation du contenu de la zonz 'titre' -->
<?php Template::startZone(); ?>
    <h1><?php echo $blog->getTitre(); ?></h1>
    <time><?php echo$blog->getDateCreation(); ?></time>
<div>
    <?php echo $blog->getTexte(); ?>
</div>
<?php Template::stopZone('content'); ?>