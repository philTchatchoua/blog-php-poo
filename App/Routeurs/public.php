<?php 
/*
    ../App/Routeurs/public.php
*/

    if(isset($_GET['pageId'])):
        include_once'../App/Routeurs/pages.php';

    elseif(isset($_GET['actualiteId'])):
        include_once'../App/Routeurs/actualites.php';
        
    elseif (isset($_GET['blogId'])) :
        include_once'../App/Routeurs/blogs.php';
    else:
        /**
         * Route par default
         * Détail de la page 1
         * PATTERN : /
         * CTRL: pageControleur
         * ACTION: showAction
         */
        $Ctrl = new \App\Controleurs\PagesControleur();
        $Ctrl->showAction();
    endif;
   