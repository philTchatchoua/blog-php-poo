<?php

/**
 *   ../App/Routeurs/public.php
 *   Routes des pages
 *   PREFIXE: : /page/
 */

$Ctrl = new \App\Controleurs\PagesControleur();
$Ctrl->showAction($_GET['pageId']);

// Ici si la pageId  = 4 on fait appale à la liste des blogs   
if($_GET['pageId']== 4):
    $ctrl = new \App\Controleurs\BlogsControleur();
    $ctrl ->indexAction();
endif;
