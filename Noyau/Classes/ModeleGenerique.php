<?php

/*
    ../Noyau/Classes/ClasseGenerique.php
    - Classes  - classe generique
    - function{
        * un constructeur
        * un hydrateur
    }

*/

namespace Noyau\Classes;

abstract class ModeleGenerique
{
    //Attribut
        protected $_dateCreation;

    //constructeur
    public function __construct(array $data = null)
    {  
        $this->hydrateur($data);
    
    }

    //GETTERS
    public function getDateCreation(string $format = 'l d M Y h:m:s')
    {
        return $this->_dateCreation ->format($format);
    }

    //SETTERS
    public function setDateCreation(string $data =null)
    {
        $this->_dateCreation = new \DateTime($data);
    }

    //Methode
    public function hydrateur(array $data = null)
    {
        if ($data) :
            foreach ($data as $propriete => $value) {
                $nomMethod = 'set' . ucfirst($propriete);
                if (method_exists($this, $nomMethod)) :
                    $this->$nomMethod($value);
                endif;
            }
        endif;
    }
}
