<?php 
/*
    ../Noyau/Classes/App.php
    - Classe de l'application
*/

namespace Noyau\Classes;

abstract class App {
    private static $_connexion = null;
    private static $_Root;
    private static $_start = false;


    //GETTERS
        public static function getConnexion()
        {
            return SELF::$_connexion;
        }

        public static  function getRoot()
        {
            return SELF:: $_Root;
        }

        public static function getTemplate()
        {
            require_once '../App/Vues/Templates/default.php';
        }

    //SETTERS
        private static function setRoot()
        {
            $local_path = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            //Redirection vers la page d'acceuil 	
            SELF::$_Root = 'http://' . $_SERVER['HTTP_HOST']. $local_path;
        }

        private static function setConnexion()
        {
            //Instantiation de l'oblet connexion

            //Paramètres de connexion
            $dsn = "mysql:host=" . DBHOTE . ";dbname=" . DBNAME;
            $param = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            // création de l'objet PDO $connexion
            try {
                SELF::$_connexion = new \PDO($dsn, DBUSER, DBPWD, $param);

            } catch (\PDOException $e) {
                die("Prolème de connexion à la base de données....");
            }

        }

    //Autres methodes
        public static function start()
        {   
            if(SELF::$_start==false):
                session_start();
                SELF::setConnexion();
                SELF::setRoot();
                SELF::$_start = true;
            endif;
        }

        public static function close()
        {
            SELF::$_connexion = null;
        }

    //modification format date
        public static function formater_date(string $date, string $format = 'D d M Y h:m:s'): string
        {
            return date_format(date_create($date), $format);
        }
            
}