<?php 
/*
    ../Noyau/Classes/GestionnaireGenerique.php
    - Classe GestionaireGenerique
*/

namespace Noyau\Classes;

abstract class GestionnaireGenerique{

    //ATTRIBUTS
    protected $_table, $_classe;

    //Construct
    protected function __construct()
    {
        $this->_classe = '\App\Modeles\\' . ucfirst(substr($this->_table, 0, -1));
    }

    //Methodes CRUD
    public function findOneById( int $data)
    {
       
        $sql = "SELECT *
               FROM `{$this->_table}`
               WHERE id = :id;";
        $rs = App::getConnexion()->prepare($sql);
        $rs->bindvalue(':id', $data, \PDO::PARAM_INT);
        $rs->execute();
        return new $this->_classe($rs->fetch(\PDO::FETCH_ASSOC));
    }

    public function findAll(array $data =[])
    {

      
        $sql = "SELECT *
                FROM `{$this->_table}`
                ORDER BY :orderBy :orderBySens";
        $rs = App::getConnexion()->prepare($sql);
        $rs->bindvalue(':orderBy',$data['orderBy'],\PDO::PARAM_STR);
        $rs->bindvalue(':orderBySens', $data['orderBySens'], \PDO::PARAM_STR);
        $rs->execute();
        $tab = $rs->fetchAll(\PDO::FETCH_ASSOC);

        return $this->fromAssocToObject($tab);
    }

    //AUTRES METHODES
    protected function fromAssocToObject(array $rs)
    {
        $tab = [];
        foreach ($rs as $r) :
            $tab[] = new $this->_classe($r);
        endforeach;
        return $tab;
    }

}