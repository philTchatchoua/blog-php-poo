<?php 
/**
 *  ../Noyau/Classes/ControleurGenerique.php
 *  Controleur - generiquue
 */

namespace Noyau\Classes;

 abstract class ControleurGenerique {
    //attributs
    protected $_gestionnaire, $_tableName;

    //constructeur
    public function __construct()
    {  
        $gestionnaireName = '\App\Modeles\\'.ucfirst($this->_tableName).'Gestionnaire';  
        $this->_gestionnaire = new $gestionnaireName();
    }

    //Methodes
    public function showAction(int $data = 1, string $field ='id')
    {
        $r= substr($this->_tableName,0,-1);
        $methodName = 'findOneBy'.ucfirst($field);
        $$r = $this->_gestionnaire->$methodName($data);

        //je charge la vue dans la variable $content
        include '../App/Vues/'.ucfirst($this->_tableName).'/show.php';
       
    }

    public function indexAction(array $UserData = [])
    {   
        $defaultValue= [
            'view'=>'index',
            'orderBy' =>'id',
            'orderBySens' =>'ASC'
        ];
        $data = array_merge($defaultValue, $UserData);
        $r =$this->_tableName;
        $$r= $this->_gestionnaire->findAll($data);
        include '../App/Vues/' . ucfirst($this->_tableName) . '/'.$data['view'].'.php';
    }

 }